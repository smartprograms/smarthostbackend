export interface IUserSettings {
	channelid: number
	highPriorityChannelList: IMixerChannelPrioritized[]
	channelList: IMixerChannelPrioritized[]
	randomChannelOrder: boolean
	teamList: IMixerTeam[]
	randomTeamOrder: boolean
	hostCategory: IHostCategory[]
	hostChanging: IHostChanging
	hostMessaging: IMessageSettings
	hostStatus: IHostStatus
	previousHostStatus: IHosting[]
	active: boolean // is hosting active?

	channelExclusion: IMixerChannel[]
}

interface IMixerChannel {
	channelid: number
	token: string
}

interface IMixerChannelPrioritized extends IMixerChannel {
	priority: number
}

interface IMixerTeam {
	name: string
	id: number
	priority: number
}

interface IHostChanging {
	rehostTimeout: number // Time in minutes per host
	rehostManual: boolean // Rehost if manually hosted someone?
	hostAfterOfflineTimeout: number // Time in minutes to delay hosting after user is offline
	maxRating: rating // max rating to host
	hostGames: IMixerGame[]
	useGamesList: boolean
}

interface IMixerGame {
	typeid: number
	name: string
}

interface IMessageSettings {
	message: string
	sendMessage: boolean
	endMessage: string
	sendEndMessage: boolean
	shouldWhisper: boolean
}

export interface IHostStatus {
	hosted: number
	username?: string
	hostedAt: number
	wasManual: boolean
	offlineAt?: number
}

export interface IHostingStatus {
	currentlyHosted: IHosting
}

export interface IHosting {
	channelid: number
	username: string | null
	manual: boolean
	hostedAt: number
}

export interface IUserStatus {
	hosted: IHosting
	stream: IStreamStatus
}

interface IStreamStatus {
	online: boolean
	offlineAt: number
}

export type IHostCategory = { key: IHostCategories; name: string; priority: number }

type IHostCategories =
	| 'HIGHPRIORITYCHANNELLIST'
	| 'CHANNELLIST'
	| 'TEAMLIST'
	| 'FOLLOWEDPARTNERS'
	| 'FOLLOWING'
	| 'PARTNERS'

type rating = '18+' | 'teen' | 'family-friendly'
