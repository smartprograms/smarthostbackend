export interface IUser {
	stripeId?: string
	admin?: boolean
	banned?: boolean
}

export interface IMixerUser extends IUser, IMixerUserProperties {
	tokens: IMixerUserTokens
}

export interface IMixerUserProperties {
	username: string
	channelid: number
	userid: number
	email: string
}

export interface IMixerUserTokens {
	access: string
	refresh: string
	expires: number
}
