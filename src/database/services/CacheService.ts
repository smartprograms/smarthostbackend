import * as NodeCache from 'node-cache'

export class CacheService {
	private cache: NodeCache

	constructor (ttlSeconds) {
		this.cache = new NodeCache({ stdTTL: ttlSeconds, checkperiod: ttlSeconds * 0.2 })
	}

	get (key) {
		const value = this.cache.get(key)
		if (value) return Promise.resolve(value)
		return Promise.reject('Not in cache')
	}

	set (key, value) {
		this.cache.set(key, value)
	}

	del (keys) {
		this.cache.del(keys)
	}

	delStartWith (startStr = '') {
		if (!startStr) {
			return
		}

		const keys = this.cache.keys()
		for (const key of keys) {
			if (key.indexOf(startStr) === 0) {
				this.del(key)
			}
		}
	}

	flush () {
		this.cache.flushAll()
	}
}
