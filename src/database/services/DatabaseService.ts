import * as AWS from 'aws-sdk'
import * as _ from 'lodash'
import cryptr = require('cryptr')
import { CacheService } from './CacheService'
import { IMixerUser } from '../models/UserModel'
import { IUserSettings, IHostStatus, IHosting } from '../models/SettingsModel'
import { userUpdated } from '../../services/hosting/AppManager'

const crypt = new cryptr(process.env.CRYPT_SECRET)

AWS.config.update({
	region: process.env.AWS_REGION
})

const dbClient = new AWS.DynamoDB.DocumentClient({
	retryDelayOptions: {
		base: 1000
	}
})

const TableName = 'MixerUsers'
const SettingsTableName = 'MixerUserSettings'

const ttl = 60 * 60 * 6
const cache = new CacheService(ttl)

export const db = {
	addOrUpdateUser (Item: IMixerUser, callback: Function) {
		try {
			const key = 'getUserById_' + Item.channelid

			Item.tokens.access = crypt.encrypt(Item.tokens.access)
			Item.tokens.refresh = crypt.encrypt(Item.tokens.refresh)

			const updateUser = () => {
				try {
					dbClient.put(
						{
							TableName,
							Item,
							ReturnValues: 'NONE'
						},
						(err) => {
							if (err) throw err

							updateCacheForUser(Item)
							userUpdated(Item)

							Item.tokens.access = crypt.decrypt(Item.tokens.access)
							Item.tokens.refresh = crypt.decrypt(Item.tokens.refresh)

							callback(null, Item)
						}
					)
				} catch (error) {
					console.error('Tried to update user got error. User:', JSON.stringify(Item))
					callback(error)
				}
			}

			cache
				.get(key)
				.then((user: IMixerUser) => {
					if (typeof Item.stripeId !== 'string' && typeof user.stripeId === 'string') Item.stripeId = user.stripeId
					if (typeof Item.admin !== 'boolean' && typeof user.admin === 'boolean') Item.admin = user.admin
					if (_.isEqual(user, Item)) {
						user.tokens.access = crypt.decrypt(user.tokens.access)
						user.tokens.refresh = crypt.decrypt(user.tokens.refresh)

						callback(null, user)
					} else updateUser()
				})
				.catch(() => {
					db
						.getUserById(Item.channelid)
						.then((user) => {
							if (typeof Item.stripeId !== 'string' && typeof user.stripeId === 'string') Item.stripeId = user.stripeId
							if (typeof Item.admin !== 'boolean' && typeof user.admin === 'boolean') Item.admin = user.admin

							if (_.isEqual(user, Item)) {
								user.tokens.access = crypt.decrypt(user.tokens.access)
								user.tokens.refresh = crypt.decrypt(user.tokens.refresh)

								callback(null, user)
							} else updateUser()
						})
						.catch(() => updateUser())
				})
		} catch (err) {
			console.error('Tried to update user got error. User:', JSON.stringify(Item))

			callback(err)
		}
	},

	getUserById (channelid: number): Promise<IMixerUser> {
		return new Promise(async (resolve, reject) => {
			try {
				const key = 'getUserById_' + channelid
				const user = (await cache.get(key)) as IMixerUser

				user.tokens.access = crypt.decrypt(user.tokens.access)
				user.tokens.refresh = crypt.decrypt(user.tokens.refresh)

				resolve(user)
			} catch (_) {
				try {
					dbClient.get(
						{
							TableName,
							Key: {
								channelid
							}
						},
						(err, data) => {
							if (err) return reject(err)

							if (!data.Item) return reject('No data?')

							const Item = data.Item as IMixerUser

							updateCacheForUser(Item)

							Item.tokens.access = crypt.decrypt(Item.tokens.access)
							Item.tokens.refresh = crypt.decrypt(Item.tokens.refresh)

							return resolve(Item)
						}
					)
				} catch (error) {
					console.error('Error getting user by id', error)
					return reject(error)
				}
			}
		})
	},

	async getAllUsers (callback: Function) {
		const getUsers = async () => {
			const params: any = {
				TableName
			}

			let scanResults = []
			let items

			try {
				do {
					items = await dbClient.scan(params).promise()
					items.Items.forEach((item) => {
						updateCacheForUser(item, true)

						scanResults.push(item)
					})
					params.ExclusiveStartKey = items.LastEvaluatedKey
				} while (items.LastEvaluatedKey)

				cache.set('getAllUsers', scanResults)

				await Promise.all(
					scanResults.map((Item) => {
						Item.tokens.access = crypt.decrypt(Item.tokens.access)
						Item.tokens.refresh = crypt.decrypt(Item.tokens.refresh)
					})
				)

				callback(null, scanResults)
			} catch (error) {
				callback(error)
			}
		}

		try {
			const key = 'getAllUsers'
			const users = (await cache.get(key)) as IMixerUser[]

			await users.forEach((Item, index) => {
				users[index].tokens.access = crypt.decrypt(Item.tokens.access)
				users[index].tokens.refresh = crypt.decrypt(Item.tokens.refresh)
			})

			callback(null, users)
		} catch (_) {
			getUsers()
		}
	},

	addOrUpdateUserSettings (Item: IUserSettings, callback: Function) {
		const key = 'getUserSettingsById_' + Item.channelid

		const updateSettings = () => {
			try {
				const req = dbClient.put({
					TableName: SettingsTableName,
					Item,
					ReturnValues: 'NONE'
				})

				req
					.on('success', () => {
						cache.set(key, Item)

						db
							.getUserById(Item.channelid)
							.then((user) => {
								userUpdated(user)
							})
							.catch((err) => console.error(err))

						callback(null, Item)
					})
					.on('error', (err) => {
						console.error('Tried to update user settings got error. Settings:', JSON.stringify(Item))

						callback(err)
					})
					.send()
			} catch (error) {
				console.error('Tried to update user settings got error. Settings:', JSON.stringify(Item))

				callback(error)
			}
		}

		cache
			.get(key)
			.then((settings: IUserSettings) => {
				if (_.isEqual(settings, Item)) return callback(null, settings)
				else updateSettings()
			})
			.catch(() => updateSettings())
	},

	async updateUserHostStatus (
		channelid: number,
		hostStatus: IHostStatus,
		prevHostStatus: IHosting[],
		callback: Function
	) {
		const key = 'getUserSettingsById_' + channelid

		const updateSettings = () => {
			try {
				const req = dbClient.update({
					TableName: SettingsTableName,
					Key: {
						channelid
					},
					UpdateExpression:
						'set hostStatus.hosted = :hosted, hostStatus.wasManual = :wasManual, hostStatus.offlineAt = :offlineAt, hostStatus.hostedAt = :hostedAt, hostStatus.username = :username, previousHostStatus = :prevStatus',
					ExpressionAttributeValues: {
						':hosted': hostStatus.hosted,
						':wasManual': hostStatus.wasManual,
						':offlineAt': hostStatus.offlineAt,
						':hostedAt': hostStatus.hostedAt,
						':username': hostStatus.username,
						':prevStatus': prevHostStatus
					},
					ReturnValues: 'ALL_NEW'
				})

				req
					.on('success', (res) => {
						const data = res.data

						if (!data) throw 'No data'

						cache.set(key, data.Attributes)

						callback(null, data.Attributes)
					})
					.on('error', (err) => {
						console.error('Tried to update user host status got error. Status:', JSON.stringify(hostStatus))
						console.error(err)

						throw err
					})
					.send()
			} catch (error) {
				console.error('Tried to update user host status got error. Status:', JSON.stringify(hostStatus))

				callback(error)
			}
		}

		cache
			.get(key)
			.then((settings: IUserSettings) => {
				if (_.isEqual(settings.hostStatus, hostStatus)) return callback(null, settings)
				else updateSettings()
			})
			.catch(() => updateSettings())
	},

	getUserSettingsById (channelid: number): Promise<IUserSettings> {
		return new Promise(async (resolve, reject) => {
			const key = 'getUserSettingsById_' + channelid

			try {
				const settings = (await cache.get(key)) as IUserSettings
				resolve(settings)
			} catch (err) {
				dbClient.get(
					{
						TableName: SettingsTableName,
						Key: {
							channelid
						}
					},
					(err, data) => {
						if (err) return reject(err)

						if (!data.Item) {
							db.addOrUpdateUserSettings(defaultUserSettings(channelid), (err, response) => {
								if (err) return reject(err)
								else resolve(response)
							})
						} else {
							const Item = data.Item as IUserSettings
							const newSettings = _.merge(defaultUserSettings(channelid), Item)

							if (_.isEqual(Item, newSettings)) {
								cache.set(key, newSettings)

								resolve(newSettings)
							} else {
								db.addOrUpdateUserSettings(newSettings, () => {
									cache.set(key, newSettings)

									resolve(newSettings)
								})
							}
						}
					}
				)
			}
		})
	}
}

async function updateCacheForUser (user, fromGetAll = false) {
	try {
		const keyId = 'getUserById_' + user.channelid
		cache.set(keyId, user)

		if (!fromGetAll) {
			const keyAllUsers = 'getAllUsers'
			const cachedUsers = (await cache.get(keyAllUsers)) as Array<{ [key: string]: any }>
			const arrayIndex = cachedUsers.findIndex((cachedUser) => cachedUser.channelid === user.channelid)

			if (arrayIndex === -1) {
				cachedUsers.push(user)
			} else {
				cachedUsers[arrayIndex] = user
			}

			cache.set(keyAllUsers, cachedUsers)
		}
	} catch (_) {}
}

export function defaultUserSettings (channelid: number) {
	const settings: IUserSettings = {
		channelid,
		highPriorityChannelList: [],
		channelList: [],
		randomChannelOrder: false,
		teamList: [],
		randomTeamOrder: false,
		hostCategory: [],
		hostChanging: {
			hostAfterOfflineTimeout: 5,
			rehostManual: false,
			rehostTimeout: 0,
			maxRating: '18+',
			hostGames: [],
			useGamesList: false
		},
		hostMessaging: {
			message: 'Hello %user%! This is just an auto-host, keep my community entertained! :D',
			sendMessage: true,
			endMessage: 'Thanks for entertaining my people!',
			sendEndMessage: false,
			shouldWhisper: true
		},
		hostStatus: {
			hosted: null,
			wasManual: false,
			hostedAt: null,
			offlineAt: Date.now()
		},
		active: false,
		channelExclusion: [],
		previousHostStatus: []
	}

	return settings
}
