import { initWeb } from './services/web/web'
import { initAppManager, stopAll } from './services/hosting/AppManager'

initWeb()

if (process.env.NODE_ENV !== 'dev') initAppManager()

process.on('SIGINT', () => {
	shutdown()
})

process.on('SIGTERM', () => {
	shutdown()
})

async function shutdown () {
	await stopAll()

	process.exit(0)
}
