import { CronJob } from 'cron'
import { apiRequest } from '../requests'
import { db } from '../../database/services/DatabaseService'

const job = new CronJob('0 0 * * 3', getPixelUser)
job.start()

const PixelID = 60672
export async function getPixelUser () {
	try {
		const settings = await db.getUserSettingsById(PixelID)

		const users = await getUsers()

		settings.active = true
		settings.channelList = users as any

		db.addOrUpdateUserSettings(settings, (err) => {
			if (err) throw err
		})
	} catch (error) {
		console.error('PIXEL-ERROR', error)
	}
}

const uri = process.env.PIXEL
function getUsers () {
	return new Promise((resolve, reject) => {
		apiRequest(
			{
				method: 'GET',
				uri
			},
			(err, res) => {
				if (err) {
					return reject('Error getting users. Error: ' + err)
				}

				if (typeof res !== 'object' || !Array.isArray(res)) {
					return reject('Response was not an array. Response: ' + typeof res === 'object' ? JSON.stringify(res) : res)
				}

				return resolve(handleUsers(res))
			}
		)
	})
}

function handleUsers (res) {
	return new Promise((resolve, reject) => {
		let users: any[] = res.filter((user) => {
			return typeof user.channelId === 'number' && typeof user.username === 'string'
		})

		if (users.length === 0) {
			return reject(
				'No users in the list after filter. Possible wrong response? Response: ' + typeof res === 'object'
					? JSON.stringify(res)
					: res
			)
		}

		users = users.map((user, priority) => {
			return { channelid: user.channelId, token: user.username, priority }
		})

		return resolve(users)
	})
}
