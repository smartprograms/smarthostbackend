import { IMixerUser } from '../../database/models/UserModel'
import {
    IUserStatus,
    IUserSettings,
    IHostingStatus
} from '../../database/models/SettingsModel'
import { findUser, getValidatedSettings } from './HelperFunctions'
import { db } from '../../database/services/DatabaseService'
import { apiRequest } from '../requests'
import ConstellationManager from './ConstellationManager'
import logger from '../log'
import * as _ from 'lodash'
import * as ws from 'ws'
import {
    UnauthorizedChatError,
    NoUserError,
    MixerError,
    RefreshTokenNotValidError,
    UserOnlineError,
    UserError,
    AppOfflineError
} from './errors'

const Constellation = new ConstellationManager()

export default class App {
    constructor(public user: IMixerUser) {
        this.startup()
    }

    /*
     *                      APP SERVICE
     *
     *   Handle app related things (Startup, shutdown, status, etc.)
     */

    // Get app status (Running or not)
    private isRunning = false
    public get appStatus(): boolean {
        return this.isRunning
    }

    // Start the app
    public async startup() {
        //If app is already running do not re-run
        if (this.isRunning) return

        try {
            this.logMessage('INFO App is now starting up')
            this.isRunning = true

            // Get current user from database to ensure most up to date auth tokens before creating client
            await this.getCurrentUser()

            // Check if user has even activated the app
            await this.getCurrentUserSettings()
            if (!this.settings.active)
                throw new UserError('Hosting is not set to active')

            // Ensure refresh token is active & Refresh tokens
            await this.introspect(this.user.tokens.refresh)
            // await this.refreshTokens() -- Do not refresh on start?

            // set past host list size (Just manually set this for now -- I do not feel like checking stuff for setting it to a real number lol)
            this.pastHostSize = 100

            // ensure all the proper status things are set
            this.setUserHostStatus(
                {
                    channelid: this.settings.hostStatus.hosted,
                    username: this.settings.hostStatus.username,
                    manual: this.settings.hostStatus.wasManual,
                    hostedAt: this.settings.hostStatus.hostedAt,
                    offlineAt: this.settings.hostStatus.offlineAt
                },
                false // Do not update db as its already in the db :P
            )

            // Start listening to the live updates & Get users current status
            this.startConstellation()
            await this.getCurrentUserStatus() // Will update db and user status if db was wrong

            this.startHosting()
        } catch (error) {
            this.logMessage(
                'ERROR Could not start up the app due to an error:',
                error
            )

            this.shutdown()
        }
    }

    // Stop the app
    public shutdown() {
        if (this.isRunning) {
            this.logMessage('INFO App is now shutting down')
            this.isRunning = false

            this.clearAllTimeouts()
            this.stopConstellation()
        }
    }

    // Log a message using logz.io & send to console
    public logMessage(...args) {
        let message = ''
        args.forEach((msg) => {
            message +=
                typeof msg !== 'object'
                    ? msg
                    : Array.isArray(msg)
                    ? '[ ' + msg.join(', ') + ' ]'
                    : JSON.stringify(msg)
            message += ' '
        })
        message = message.trim()

        let level: string
        switch (message.split(' ')[0]) {
            case 'ERROR':
                level = 'ERROR'
                message.replace(level, '')
                break
            case 'DEBUG':
                level = 'DEBUG'
                message.replace(level, '')
                break
            case 'DEBUG-ERROR':
                message.replace(level, '')
                level = 'DEBUGERROR'
                break
            default:
                level = 'INFO'
                break
        }

        const obj = {
            level,
            message,
            user: this.user.username
        }

        logger.log(obj)
        console.log(JSON.stringify(obj))
    }

    /*
     *                          USER SERVICE
     *
     *   Handle user related stuff (Settings, user account in database, etc.)
     */

    private currentUserStatus: IUserStatus = {
        hosted: {
            channelid: null,
            username: null,
            hostedAt: null,
            manual: false
        },
        stream: {
            online: false,
            offlineAt: null
        }
    }
    public get userStatus(): IUserStatus {
        return this.currentUserStatus
    }

    private hostStatus: IHostingStatus = {
        currentlyHosted: {
            channelid: null,
            hostedAt: null,
            manual: null,
            username: null
        }
    }
    public get userHostStatus(): IHostingStatus {
        return this.hostStatus
    }

    restrictedUsers: number[] = []
    public dontHostList(ignorePastHostList): number[] {
        const list = [...this.settings.channelExclusion].map(
            (channel) => channel.channelid
        )
        return ignorePastHostList
            ? [...list, this.settings.hostStatus.hosted]
            : list.concat(
                  [...this.settings.previousHostStatus].map(
                      (user) => user.channelid
                  )
              )
    }

    private addRestrictedHost(channel) {
        this.logMessage(
            'DEBUG addRestrictedHost(channel) -> Adding a user to the exclusion list. User:',
            channel
        )

        this.settings.channelExclusion.push(channel)

        db.addOrUpdateUserSettings(this.settings, (err) => {
            if (err)
                this.logMessage(
                    'ERROR addRestrictedHost(channel) -> Failed to save channel exclusion to database. Error:',
                    err
                )
        })
    }

    public get gamesList() {
        return this.settings.hostChanging.useGamesList
            ? this.settings.hostChanging.hostGames
            : []
    }

    // Get the current user status from Mixer and ensure accuracy of our "known" user status
    public async getCurrentUserStatus() {
        try {
            this.logMessage(
                "INFO getCurrentUserStatus() -> Updating the user's status."
            )

            const uri =
                'https://mixer.com/api/v1/channels/' +
                this.user.channelid +
                '?fields=hosteeId,online'

            const opts = {
                method: 'GET',
                uri,
                headers: {
                    'Client-ID': process.env.MIXER_CLIENTID
                }
            }

            const [err, { hosteeId, online }] = (await to(
                this.clientRequest(opts)
            )) as [any, { hosteeId: number; online: boolean }]

            if (err) {
                this.logMessage(
                    'ERROR getCurrentUserStatus() -> Could not get updated user status from Mixer. Error:',
                    err
                )

                throw err
            }

            if (this.currentUserStatus.hosted.channelid !== hosteeId)
                this.handleHostedUser({ hosteeId }, false)
            if (this.currentUserStatus.stream.online !== online)
                this.handleOnlineStatus({ online }, false)

            return Promise.resolve(this.currentUserStatus)
        } catch (error) {
            return Promise.reject(error)
        }
    }

    // Get the most up to date user from the database (Gets any updated tokens from relogging in on the site)
    public async getCurrentUser() {
        try {
            this.logMessage('INFO getCurrentUser() -> Updating the user')

            const oldTokens = this.user.tokens
            let [err, user] = await to(db.getUserById(this.user.channelid))

            if (err) {
                this.logMessage(
                    'ERROR getCurrentUser() -> Could not get updated user from database. Error:',
                    err
                )

                throw err
            }

            if (!user || !user.tokens || !user.tokens.refresh) {
                this.logMessage(
                    'ERROR getCurrentUser() -> User, or their tokens are not defined. User:',
                    user
                )

                throw new NoUserError(
                    'No user, or their tokens are not defined'
                )
            }

            if (user.banned) {
                this.logMessage(
                    'ERROR getCurrentUser() -> User is banned, can not get their account. User:',
                    user
                )

                throw new UserError('User is banned')
            }

            this.user = user

            if (_.isEqual(oldTokens, this.user.tokens))
                return Promise.resolve(this.user)

            this.logMessage(
                'DEBUG-ERROR getCurrentUser() -> Tokens from database and app were not the same, checking which one is active.'
            )

            let [tokenError] = await to(
                this.introspect(this.user.tokens.refresh)
            )

            if (tokenError)
                this.logMessage(
                    'ERROR getCurrentUser() -> New tokens are bad. Error:',
                    tokenError
                )

            if (!tokenError) return Promise.resolve(this.user)

            let [tokenError2] = await to(this.introspect(oldTokens.refresh))

            if (tokenError2) {
                this.logMessage(
                    'ERROR getCurrentUser() -> Old tokens are also bad. Error:',
                    tokenError2
                )
                throw tokenError2
            }

            this.user.tokens = oldTokens

            return Promise.resolve(this.user)
        } catch (error) {
            return Promise.reject(error)
        }
    }

    public async userSettingsUpdated() {
        const old = this.settings

        let [err, settings] = await to(this.getCurrentUserSettings())

        if (err) {
            this.logMessage(
                'ERROR userSettingsUpdated() -> Could not get users updated settings. Error:',
                err
            )

            return
        }

        this.settings = settings

        if (this.appStatus !== this.settings.active) {
            if (this.settings.active) this.startup()
            else this.shutdown()
        }

        if (this.appStatus) {
            if (
                old.hostChanging.rehostTimeout !==
                this.settings.hostChanging.rehostTimeout
            ) {
                if (
                    (this.currentUserStatus.hosted.manual &&
                        this.settings.hostChanging.rehostManual) ||
                    !this.currentUserStatus.hosted.manual
                )
                    this.setRehostTimeout()
            }
        }
    }

    // Get the current users most up to date settings from database
    private settings: IUserSettings
    public async getCurrentUserSettings() {
        try {
            this.logMessage(
                'DEBUG getCurrentUserSettings() -> Getting current users settings'
            )
            this.settings = getValidatedSettings(
                await db.getUserSettingsById(this.user.channelid)
            )

            return Promise.resolve(this.settings)
        } catch (error) {
            this.logMessage(
                'ERROR getCurrentUserSettings() -> Could not get users updated settings. Error:',
                error
            )

            return Promise.reject(error)
        }
    }

    private pastHostSize = 10
    private setUserHostStatus(
        { channelid, username, manual, hostedAt, offlineAt },
        updateDB = true
    ) {
        username = username || null

        this.logMessage(
            'DEBUG setUserHostStatus() -> Setting user status. Status:',
            {
                channelid,
                username,
                manual,
                hostedAt,
                offlineAt
            }
        )

        this.currentUserStatus.hosted = {
            channelid,
            username,
            manual,
            hostedAt
        }
        this.hostStatus.currentlyHosted = this.currentUserStatus.hosted

        this.currentUserStatus.stream = {
            online: offlineAt === null,
            offlineAt: offlineAt
        }

        this.settings.hostStatus = {
            hosted: channelid,
            username,
            wasManual: manual,
            hostedAt,
            offlineAt
        }

        const inList =
            this.settings.previousHostStatus.find(
                (channel) => channel.channelid === channelid
            ) !== undefined

        if (
            this.settings.previousHostStatus.length >= this.pastHostSize &&
            !inList
        )
            this.settings.previousHostStatus.shift()
        if (!inList)
            this.settings.previousHostStatus.push(this.currentUserStatus.hosted)

        if (updateDB)
            db.updateUserHostStatus(
                this.user.channelid,
                this.settings.hostStatus,
                this.settings.previousHostStatus,
                (err) => {
                    if (err)
                        this.logMessage(
                            'ERROR setUserHostStatus() -> Host status could not be updated in database. Error:',
                            err
                        )
                    else
                        this.logMessage(
                            'DEBUG setUserHostStatus() -> Host status is updated in database successfully.'
                        )
                }
            )
    }

    // Get the hosting categories sorted by priority
    public get hostCategories() {
        return this.settings.hostCategory.sort(
            (a, b) => a.priority - b.priority
        )
    }

    // Get the channels list sorted by priority
    public get channelsList() {
        if (!this.settings.channelList) this.settings.channelList = []

        if (this.settings.randomChannelOrder)
            return _.shuffle(this.settings.channelList)
        else
            return this.settings.channelList.sort(
                (a, b) => a.priority - b.priority
            )
    }

    public get priorityChannelsList() {
        if (!this.settings.highPriorityChannelList)
            this.settings.highPriorityChannelList = []

        return this.settings.highPriorityChannelList.sort(
            (a, b) => a.priority - b.priority
        )
    }

    // Get the teams list sorted by priority
    public get teamsList() {
        if (this.settings.randomTeamOrder)
            return _.shuffle(this.settings.teamList)
        else
            return this.settings.teamList.sort(
                (a, b) => a.priority - b.priority
            )
    }

    // Get the max rating to host
    public get maxHostRating() {
        return this.settings.hostChanging.maxRating
    }

    public async clientRequest(options) {
        return new Promise((resolve, reject) => {
            apiRequest(options, (err, body) => {
                if (err) return reject(err)

                if (!body) return reject('Body is not defined')

                return resolve(body)
            })
        })
    }

    /*
     *     AUTHENTICATION SERVICE
     *
     *   Handle users authentication
     */

    // Refresh tokens if the refresh token is active
    private async refreshTokens() {
        try {
            if (!this.isRunning)
                throw new AppOfflineError(
                    'App is not supposed to be running - Wont refresh tokens'
                )

            this.logMessage('DEBUG refreshTokens() -> Refreshing users tokens.')

            if (!this.user || !this.user.tokens || !this.user.tokens.refresh) {
                this.logMessage(
                    'ERROR refreshTokens() -> No user or refresh token to refresh.'
                )

                throw new NoUserError('No user or refresh token to refresh')
            }

            const opts = {
                method: 'POST',
                uri: `https://mixer.com/api/v1/oauth/token?c=${Date.now()}`,
                headers: {
                    'Client-ID': process.env.MIXER_CLIENTID
                },
                body: {
                    grant_type: 'refresh_token',
                    refresh_token: this.user.tokens.refresh,
                    client_id: process.env.MIXER_CLIENTID,
                    client_secret: process.env.MIXER_SECRETID
                }
            }

            const [error, response] = (await to(this.clientRequest(opts))) as [
                any,
                { access_token: string; refresh_token: string }
            ]

            if (error) {
                let emessage = error
                if (error.response) emessage = error.response

                this.logMessage(
                    'ERROR refreshTokens() -> Could not get refresh tokens. Error:',
                    emessage
                )

                throw error
            }

            if (!response) {
                this.logMessage(
                    'ERROR refreshTokens() -> Could not get refresh tokens. Error:',
                    'No response data from Mixer'
                )

                throw new MixerError('No response data from Mixer')
            }

            const { access_token, refresh_token } = response

            if (!access_token || !refresh_token) {
                this.logMessage(
                    'ERROR refreshTokens() -> Bad refresh tokens response. Response:',
                    response
                )

                throw new MixerError('Auth response is bad.')
            }

            this.user.tokens = {
                refresh: refresh_token,
                access: access_token,
                expires: Date.now() + 1000 * 60 * 60 * 6
            }

            this.logMessage(
                'INFO refreshTokens() -> Refreshing tokens was successful.'
            )

            // Update database with these tokens to ensure we wont lose authentication if app restart occurs
            db.addOrUpdateUser(this.user, (err) => {
                if (err)
                    this.logMessage(
                        'ERROR refreshTokens() -> Tokens could not be updated in database. Error:',
                        err
                    )
                else
                    this.logMessage(
                        'INFO refreshTokens() -> Tokens are updated in database successfully.'
                    )
            })

            return Promise.resolve(this.user.tokens)
        } catch (error) {
            return Promise.reject(error)
        }
    }

    // check if refresh token is active
    private async introspect(token = this.user.tokens.refresh) {
        try {
            const opts = {
                method: 'POST',
                uri: 'https://mixer.com/api/v1/oauth/token/introspect',
                headers: {
                    'Client-ID': process.env.MIXER_CLIENTID
                },
                body: {
                    token
                }
            }

            const [error, response] = (await to(this.clientRequest(opts))) as [
                any,
                { active: boolean; scope: string; token_type: string }
            ]

            if (error) {
                this.logMessage(
                    'ERROR introspect() -> Could not get introspection on token. Error:',
                    error
                )

                throw error
            }

            if (!response) {
                this.logMessage(
                    'ERROR introspect() -> Could not get introspection on token. Error:',
                    'No response data from Mixer'
                )

                throw new MixerError('No response data from Mixer')
            }

            let { active, scope, token_type } = response

            if (!active && token_type === 'refresh_token') {
                this.logMessage(
                    'ERROR introspect() -> The token is not active.'
                )

                throw new RefreshTokenNotValidError('The token is not active')
            }

            this.logMessage(
                'DEBUG introspect() -> The token is good and active. Token response: {' +
                    `active: ${active}, scopes: [${scope}], token_type: ${token_type}` +
                    '}'
            )

            return Promise.resolve({ active, scope, token_type })
        } catch (error) {
            return Promise.reject(error)
        }
    }

    /*
     *         MESSAGING SERVICE
     *
     *   Handle sending messages to user
     */

    private async sendMessage(
        message: string,
        to: string,
        channel: number,
        reconnectAttempts = 5
    ) {
        let attempt = 0

        message = message.replace(/%user%/gi, to)

        const joinChat = () => {
            ++attempt

            if (attempt > reconnectAttempts) {
                this.logMessage(
                    'ERROR sendMessage() -> Unable to send message, could not ever connect to chat. Attempts:',
                    reconnectAttempts
                )
                return
            }

            this.getChat(channel)
                .then((response) => {
                    this.createChatAndSendMessage(
                        response,
                        channel,
                        to,
                        message
                    )
                })
                .catch((error) => {
                    let emessage = error

                    this.logMessage(
                        'ERROR sendMessage() -> Could not get the chat key. Error:',
                        emessage
                    )

                    if (error.name === 'UnauthorizedChatError') {
                        this.logMessage(
                            'ERROR sendMessage() -> Unable to send message, got Unauthorized Chat Error, not trying again.'
                        )
                    } else if (error.name === 'NoUserError') {
                        this.logMessage(
                            'ERROR sendMessage() -> Unable to send message, got No User Error, not trying again.'
                        )
                    } else {
                        setTimeout(() => {
                            this.refreshTokens()
                                .then(() => {
                                    joinChat()
                                })
                                .catch(() => {
                                    this.logMessage(
                                        'ERROR sendMessage() -> Could not refresh tokens. Shutting down.'
                                    )

                                    this.shutdown()
                                })
                        }, attempt * 1000)
                    }
                })
        }

        joinChat()
    }

    private async getChat(channelid: number) {
        try {
            if (!this.user || !this.user.tokens || !this.user.tokens.refresh) {
                this.logMessage(
                    'ERROR getChat() -> No user or refresh token to connect to chat.'
                )

                throw new NoUserError(
                    'No user or refresh token to connect to chat'
                )
            }

            const opts = {
                method: 'GET',
                uri: 'https://mixer.com/api/v1/chats/' + channelid,
                headers: {
                    'Client-ID': process.env.MIXER_CLIENTID,
                    Authorization: 'Bearer ' + this.user.tokens.access
                }
            }

            const [error, response] = await to(this.clientRequest(opts))

            if (error) {
                this.logMessage(
                    'ERROR getChat() -> Error trying to get chat connection key. Error:',
                    error
                )

                throw error
            }

            if (!response.authkey) {
                this.logMessage(
                    'ERROR getChat() -> No authkey in chats response. Response:',
                    response
                )

                throw new UnauthorizedChatError('No authkey present')
            }

            return Promise.resolve(response)
        } catch (error) {
            return Promise.reject(error)
        }
    }

    private async createChatAndSendMessage(
        response: any,
        id: number,
        username: string,
        message: string
    ) {
        let packetId = 0

        let messagePacket

        const send = (type, method, args = []) => {
            const packet = {
                type,
                method,
                arguments: args,
                id: packetId++
            }

            if (method === 'whisper') messagePacket = packet.id

            socket.send(JSON.stringify(packet))
        }

        const socket = new ws(response.endpoints[0])

        socket.onopen = () => {
            send('method', 'auth', [id, this.user.userid, response.authkey])
        }

        socket.onmessage = (e) => {
            const data =
                typeof e.data === 'string' ? JSON.parse(e.data) : e.data

            if (data.type === 'reply') {
                if (data.id === messagePacket) {
                    if (data.error) {
                        this.logMessage(
                            'DEBUG-ERROR createChatAndSendMessage() -> Could not send message. Error:',
                            data.error
                        )

                        socket.close()
                    } else {
                        this.logMessage(
                            'INFO createChatAndSendMessage() -> Message sent success.'
                        )

                        socket.close()
                    }
                } else if (
                    data.data &&
                    data.data.hasOwnProperty('authenticated')
                ) {
                    if (data.data.authenticated === true) {
                        let packet = {
                            type: 'method',
                            method: 'whisper',
                            args: [username, message]
                        }

                        send(packet.type, packet.method, packet.args)
                    } else {
                        this.logMessage(
                            'ERROR createChatAndSendMessage() -> We had a key to connect to chat, however we were not authorized. Reply:',
                            data
                        )

                        socket.close()
                    }
                }
            }
        }
    }

    /*
     *                 HOSTING SERVICE
     *
     *   Handle hosting users and setting host timeouts
     */

    private startHosting() {
        if (this.currentUserStatus.stream.online) {
            this.logMessage(
                'DEBUG startHosting() -> User is online no need to set a hosting timeout.'
            )
        } else {
            this.logMessage(
                'DEBUG startHosting() -> User is currently offline. Host Status:',
                this.currentUserStatus.hosted
            )

            if (this.currentUserStatus.hosted.channelid === null) {
                this.logMessage(
                    'DEBUG startHosting() -> User is not currently hosting anyone, setting offline host timeout.'
                )

                this.setOfflineHostTimeout()
            } else if (
                !this.currentUserStatus.hosted.manual ||
                this.settings.hostChanging.rehostManual
            ) {
                this.logMessage(
                    'DEBUG startHosting() -> User is currently hosting someone, setting rehost timeout.'
                )

                this.setRehostTimeout()
            }
        }
    }

    private findingUserToHost = false
    private hostingUser: { channelid: number; username: string }
    private ignorePastHostList = false
    private async hostNewUser() {
        if (this.findingUserToHost || !this.isRunning) return

        // --await this.getCurrentUserStatus()-- <Assume constellation is sending events correctly as mixer api is slow to update (might get wrong status)>

        try {
            this.findingUserToHost = true

            // Check if user is live here before we try to find a user (This should technically never happen)
            if (this.currentUserStatus.stream.online) {
                this.logMessage(
                    'DEBUG-ERROR hostNewUser() -> User is currently online we will not try to host anyone.'
                )

                throw new UserOnlineError('User is currently online')
            }

            this.logMessage(
                'DEBUG hostNewUser() -> User status before hosting new user is:',
                this.currentUserStatus
            )

            if (this.currentUserStatus.hosted.channelid !== null)
                this.ignorePastHostList = false

            const userFound = await findUser(this, this.ignorePastHostList)

            this.findingUserToHost = false

            // While finding a user to host the user went live (This occurs normally when someone un-hosts to go live - Still should *almost* never occur)
            if (this.currentUserStatus.stream.online) {
                this.logMessage(
                    'DEBUG-ERROR hostNewUser() -> User is currently online we will not try to host the found user.'
                )

                throw new UserOnlineError('User is currently online')
            }

            if (userFound) {
                this.ignorePastHostList = false

                this.logMessage(
                    'DEBUG hostNewUser() -> We found a user to host. Found:',
                    userFound
                )

                this.hostingUser = {
                    channelid: userFound.channelid,
                    username: userFound.username
                }

                const opts = {
                    method: 'PUT',
                    uri:
                        'https://mixer.com/api/v1/channels/' +
                        this.user.channelid +
                        '/hostee',
                    headers: {
                        'Client-ID': process.env.MIXER_CLIENTID,
                        Authorization: 'Bearer ' + this.user.tokens.access
                    },
                    body: {
                        id: userFound.channelid,
                        auto: true
                    }
                }

                const response = (await this.clientRequest(opts)) as {
                    [key: string]: any
                }

                // Check if we received back the input we gave (It's mixer, so I want to see if this happens)
                if (response.hosteeId === userFound.channelid)
                    this.logMessage(
                        'DEBUG hostNewUser() -> Host PUT Success. Hosting:',
                        response.hosteeId
                    )
                // this should technically never happen... but Mixer
                else
                    this.logMessage(
                        'ERROR hostNewUser() -> Host PUT Failed? We are hosting',
                        response.hosteeId,
                        'instead of',
                        userFound.channelid
                    )

                this.handleHostedUser({ hosteeId: response.hosteeId })
            } else {
                this.logMessage(
                    'DEBUG hostNewUser() -> No user to host was found.'
                )

                if (this.settings.previousHostStatus.length > 0) {
                    this.settings.previousHostStatus = []
                    db.updateUserHostStatus(
                        this.user.channelid,
                        this.settings.hostStatus,
                        this.settings.previousHostStatus,
                        (err) => {
                            if (err)
                                this.logMessage(
                                    'ERROR hostNewUser() -> Host status could not be updated in database. Error:',
                                    err
                                )
                            else
                                this.logMessage(
                                    'INFO hostNewUser() -> Host status is updated in database successfully.'
                                )
                        }
                    )
                    this.hostUserNow()
                } else {
                    this.setRehostTimeout()
                }
            }
        } catch (error) {
            this.findingUserToHost = false

            if (error.status === 401) {
                // Tokens need to be refreshed
                const [refreshError] = await to(this.refreshTokens())

                if (refreshError) {
                    this.logMessage(
                        'DEBUG-ERROR hostNewUser() -> Could not refresh users tokens. Error:',
                        refreshError
                    )

                    return this.shutdown()
                }

                this.logMessage(
                    'DEBUG hostNewUser() -> Refreshed tokens and trying to host a user again.'
                )

                return this.hostUserNow()
            } else if (error.status === 403) {
                // User is either banned from the stream or banned or app (I assume they are banned in the stream cuz we will get refresh errors)
                this.logMessage(
                    'DEBUG-ERROR hostNewUser() -> Could not host a user due to channel being banned. Error:',
                    error
                )

                this.addRestrictedHost({
                    token: this.hostingUser.username,
                    channelid: this.hostingUser.channelid
                })

                return this.hostUserNow()
            } else if (error.status === 400) {
                if (
                    error.response &&
                    error.response.body &&
                    Array.isArray(error.response.body.details) &&
                    error.response.body.details[0].type === 'hostDisallowed' // Channel does not allow hosting
                ) {
                    if (
                        error.response.body.details[0].message &&
                        error.response.body.details[0].message.includes(
                            'live hosting'
                        ) // User is online... Mixer never updated us saying that the user went live
                    ) {
                        this.logMessage(
                            'DEBUG-ERROR hostNewUser() -> User is online we cannot host someone. Error:',
                            error
                        )

                        // Mixer did not send us a constellation update? Might need to update database
                        if (this.currentUserStatus.stream.online === false)
                            this.setUserHostStatus({
                                channelid: null,
                                username: null,
                                manual: null,
                                hostedAt: null,
                                offlineAt: Date.now()
                            })

                        return this.clearAllTimeouts()
                    } else {
                        this.logMessage(
                            'DEBUG-ERROR hostNewUser() -> Channel does not allow hosting. Error:',
                            error
                        )

                        this.addRestrictedHost({
                            token: this.hostingUser.username,
                            channelid: this.hostingUser.channelid
                        })

                        return this.hostUserNow()
                    }
                } else if (
                    error.response &&
                    error.response.body &&
                    Array.isArray(error.response.body.details) &&
                    error.response.body.details[0].type === 'hostWhileOffline'
                ) {
                    // Channel that was attempted to be hosted is now offline (This should technically never happen... but Mixer)
                    this.logMessage(
                        'DEBUG-ERROR hostNewUser() -> The user we found to host went offline. Error:',
                        error
                    )

                    return this.hostUserNow()
                } else {
                    // Unknown validation error occurred (This should technically never happen... but Mixer)
                    this.logMessage(
                        'DEBUG-ERROR hostNewUser() -> 400 request to hostee. Error:',
                        error
                    )

                    return this.setRehostTimeout()
                }
            } else if (error.message === 'User is currently online') {
                // The user went online while looking for a user
                this.logMessage(
                    'DEBUG-ERROR hostNewUser() -> User is online we cannot host someone.'
                )

                // The db should already know we are offline if we hit here no need to update it

                return this.clearAllTimeouts()
            } else {
                // Unknown error has occurred (This should technically never happen... but Mixer)
                this.logMessage(
                    'ERROR hostNewUser() -> Could not host a new user. Error:',
                    error
                )

                return this.setRehostTimeout()
            }
        }
    }

    /*
     *   Hosting Timeouts
     */

    // Get the offline timeout length
    // Min timeout 15s: Just a small buffer to not host immediately
    private get offlineTimeout(): number {
        const MIN_TIMEOUT_TIME = 15 * 1000
        let usersTimeoutTime =
            this.settings.hostChanging.hostAfterOfflineTimeout * 60000

        if (this.currentUserStatus.stream.offlineAt) {
            let offlineTime =
                Date.now() - this.currentUserStatus.stream.offlineAt

            usersTimeoutTime = usersTimeoutTime - offlineTime
        }

        return Math.max(...[MIN_TIMEOUT_TIME, usersTimeoutTime])
    }

    // Set the offline host timeout (Do not reset the offline timeout)
    private offlineTimeoutHolder: NodeJS.Timeout
    private setOfflineHostTimeout() {
        if (this.offlineTimeoutHolder) {
            this.logMessage(
                'DEBUG-ERROR setOfflineHostTimeout() -> Offline Host Timeout is already set, no need to set it again.'
            )
        } else {
            this.logMessage(
                'INFO setOfflineHostTimeout() -> Setting offline timeout. Time:',
                this.offlineTimeout
            )

            this.clearRehostTimeout()

            this.offlineTimeoutHolder = setTimeout(() => {
                // We no longer need to hold a value for the timeout since it has completed
                this.offlineTimeoutHolder = null

                this.hostNewUser()
            }, this.offlineTimeout)
        }
    }

    // Clear offline timeout
    // Needed: User goes back online, App starts shutting down, User manually hosts someone
    private clearOfflineTimeout() {
        this.logMessage(
            'DEBUG clearOfflineTimeout() -> Clearing offline timeout.'
        )
        if (this.offlineTimeoutHolder) clearTimeout(this.offlineTimeoutHolder)

        this.offlineTimeoutHolder = null
    }

    // Get the rehost timeout length
    private get rehostTimeout(): number {
        let timeout = this.settings.hostChanging.rehostTimeout * 60000

        if (timeout === 0 && this.currentUserStatus.hosted.channelid !== null) {
            return timeout
        }

        if (this.currentUserStatus.hosted.hostedAt) {
            let hostTime = Date.now() - this.currentUserStatus.hosted.hostedAt

            if (
                timeout - hostTime <= 0 ||
                this.currentUserStatus.hosted.channelid === null
            ) {
                if (this.currentUserStatus.hosted.channelid !== null)
                    timeout = Math.max(...[timeout / 5, 10 * 60000])
                // set timeout to 1/5th the rehost timeout (min 10mins)
                else timeout = 10 * 60000 // if this gets called it means a user was not found to host and no one is hosted
            } else timeout = Math.max(...[timeout - hostTime, 1 * 60000]) // wait at least 1 minute to rehost a user (the 1min min should almost never happen)
        } else if (this.currentUserStatus.hosted.channelid === null)
            timeout = 10 * 60000

        return timeout
    }

    // Set rehost user timeout if the length of the timeout is at least 30mins otherwise wait until user is offline
    private rehostTimeoutHolder: NodeJS.Timeout
    private setRehostTimeout() {
        if (this.rehostTimeout === 0) {
            this.logMessage(
                'INFO setRehostTimeout() -> Rehost timeout is not being set because it is 0.'
            )

            if (this.rehostTimeoutHolder)
                this.logMessage(
                    'DEBUG-ERROR setRehostTimeout() -> There is an active timeout set for rehosting.'
                )

            return
        }

        if (this.rehostTimeoutHolder) {
            this.logMessage(
                'DEBUG-ERROR setRehostTimeout() -> Rehost Host Timeout is already set, no need to set it again.'
            )
        } else {
            this.logMessage(
                'INFO setRehostTimeout() -> Setting Rehost timeout. Time:',
                this.rehostTimeout
            )

            this.clearOfflineTimeout()

            this.rehostTimeoutHolder = setTimeout(() => {
                // We no longer need to hold a value for the timeout since it has completed
                this.rehostTimeoutHolder = null

                this.hostNewUser()
            }, this.rehostTimeout)
        }
    }

    // Host a user now without using rehost timeout or offline timeout
    private tempTimeout: NodeJS.Timeout
    hostUserNow() {
        this.logMessage(
            'DEBUG hostUserNow() -> Hosting a new user now, bypassing timeouts.'
        )

        // We do not want any timeouts now
        this.clearAllTimeouts()

        this.tempTimeout = setTimeout(() => {
            this.tempTimeout = null
            this.hostNewUser()
        }, 1000 * 15) // Host in 15 seconds in case of user unhosting to manually host or go online
    }

    private clearRehostTimeout() {
        this.logMessage(
            'DEBUG clearRehostTimeout() -> Clearing rehost timeout.'
        )
        if (this.rehostTimeoutHolder) clearTimeout(this.rehostTimeoutHolder)

        this.rehostTimeoutHolder = null
    }

    // Clear all timeouts
    private clearAllTimeouts() {
        this.logMessage('DEBUG clearAllTimeouts() -> Clearing all timeouts.')

        if (this.tempTimeout) clearTimeout(this.tempTimeout)
        this.clearOfflineTimeout()
        this.clearRehostTimeout()
    }

    /*
     *     CONSTELLATION SERVICE
     *
     *   Handle constellation service
     */

    // start constellation service
    private startConstellation() {
        this.logMessage(
            'INFO startConstellation() -> Starting the constellation service.'
        )

        Constellation.subscribeTo('channel:' + this.user.channelid + ':update')

        this.constellationListener()
    }

    /*
     *   Handle constellation events
     */
    private constellationListener() {
        Constellation.on(
            'channel:' + this.user.channelid + ':update',
            (data) => {
                if (!data || typeof data !== 'object') return

                if (data.hasOwnProperty('hosteeId'))
                    this.handleHostedUser(data as { hosteeId: number })

                if (data.hasOwnProperty('online'))
                    this.handleOnlineStatus(data as { online: boolean })
            }
        )
    }

    // stop constellation service
    private stopConstellation() {
        this.logMessage(
            'INFO stopConstellation() -> Unsubscribing to constellation at stopConstellation.'
        )

        Constellation.unsubscribeTo(
            'channel:' + this.user.channelid + ':update'
        )
    }

    private async handleHostedUser({ hosteeId }, setTimeout = true) {
        if (
            this.currentUserStatus.hosted.channelid === hosteeId &&
            hosteeId !== null
        )
            return

        this.logMessage(
            'INFO handleHostedUser() -> Channel is host a new user. User:',
            hosteeId
        )

        if (this.hostingUser && this.hostingUser.channelid === hosteeId) {
            this.setUserHostStatus({
                channelid: hosteeId,
                username: this.hostingUser.username,
                manual: false,
                hostedAt: Date.now(),
                offlineAt: this.currentUserStatus.stream.offlineAt
            })

            this.hostingUser = null

            if (
                this.settings.hostMessaging.sendMessage &&
                !this.currentUserStatus.hosted.manual
            ) {
                this.logMessage(
                    'INFO handleHostedUser() -> Sending a message to hosted user. User:',
                    this.currentUserStatus.hosted.username
                )

                this.sendMessage(
                    this.settings.hostMessaging.message,
                    this.currentUserStatus.hosted.username,
                    this.currentUserStatus.hosted.channelid
                )
            }

            this.setRehostTimeout()
        } else {
            if (this.currentUserStatus.hosted.channelid !== hosteeId)
                this.setUserHostStatus({
                    channelid: hosteeId,
                    username: null,
                    manual: true,
                    hostedAt: Date.now(),
                    offlineAt: this.settings.hostStatus.offlineAt
                })

            if (hosteeId !== null) {
                this.clearAllTimeouts()

                if (this.settings.hostChanging.rehostManual && setTimeout)
                    this.setRehostTimeout()
            } else if (!this.currentUserStatus.stream.online)
                this.setOfflineHostTimeout()
        }
    }

    private handleOnlineStatus({ online }, setTimeout = true) {
        // Mixer sends online status multiple times so this will ensure we only set the online/offline status once
        if (this.currentUserStatus.stream.online === online) return

        this.logMessage(
            'INFO handleOnlineStatus() -> CONSTELLATION Channel live status changed. Status:',
            online ? 'online' : 'offline'
        )

        if (online) {
            this.clearAllTimeouts()

            this.settings.previousHostStatus = []
        }

        this.setUserHostStatus({
            channelid: online ? null : this.settings.hostStatus.hosted,
            username: online ? null : this.settings.hostStatus.username,
            manual: this.settings.hostStatus.wasManual,
            hostedAt: online ? null : this.settings.hostStatus.hostedAt,
            offlineAt: online ? null : Date.now()
        })

        if (!online && setTimeout) this.setOfflineHostTimeout()
    }
}

function to(promise) {
    return promise.then((data) => [null, data]).catch((err) => [err])
}
