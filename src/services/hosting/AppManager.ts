import { IMixerUser } from '../../database/models/UserModel'
import { db } from '../../database/services/DatabaseService'
import logger from '../log'
import App from './App'
import { asyncForEach } from './HelperFunctions'

const runningUsers: App[] = []

export function initAppManager () {
	db.getAllUsers((err, users) => {
		if (err) {
			logger.log({
				error: err,
				level: 'STARTERROR'
			})

			return
		}

		users.forEach((user) => {
			const app = runningUsers.find((app) => app.user.channelid === user.channelid)
			if (!app) return startApp(user)
		})
	})
}

export function userUpdated (user: IMixerUser) {
	const app = runningUsers.find((app) => app.user.channelid === user.channelid)

	if (app) app.userSettingsUpdated()
	else startApp(user)
}

export function getAppStatus (channelid: number) {
	const app = runningUsers.find((app) => app.user.channelid === channelid)

	if (app) return app.appStatus
	else return false
}

export function forceHost (channelid: number) {
	const app = runningUsers.find((app) => app.user.channelid === channelid)

	if (app) app.hostUserNow()
}

export function forceRestart (channelid: number) {
	const app = runningUsers.find((app) => app.user.channelid === channelid)

	if (app) {
		app.shutdown()
		setTimeout(() => app.startup(), 5000)
	}
}

export function forceShutdown (channelid: number) {
	const app = runningUsers.find((app) => app.user.channelid === channelid)

	if (app) app.shutdown()
}

function startApp (user: IMixerUser) {
	if (process.env.NODE_ENV !== 'dev') runningUsers.push(new App(user))
}

export async function stopAll () {
	await asyncForEach(runningUsers, (user) => {
		user.shutdown()
	})

	await timeout(3000)

	return true
}

function timeout (ms) {
	return new Promise((resolve) => setTimeout(resolve, ms))
}
