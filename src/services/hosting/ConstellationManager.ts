import { Client } from 'mixer-client-node'
import ConstellationService from 'mixer-client-node/dist/Mixer.Base/Services/ConstellationService'
import { EventEmitter } from 'events'

export default class ConstellationManager extends EventEmitter {
	private client: Client
	private constellation: ConstellationService
	constructor () {
		super()

		this.client = new Client({
			clientid: process.env.MIXER_CLIENTID
		})

		this.constellation = this.client.constellationService

		this.const_listeners()
	}

	const_listeners () {
		this.constellation.on('error', console.error)
		this.constellation.on('warning', console.error)
		this.constellation.on('reply', (data) => {
			if (data.error) console.error(data)
		})

		this.constellation.on('event', (data, name) => {
			this.emit(name, data)
		})
	}

	subscribeTo (event) {
		if (!this.constellation.subscribedEvents.includes(event)) this.client.subscribeTo(event)
	}

	unsubscribeTo (event) {
		if (this.constellation.subscribedEvents.includes(event)) this.client.unsubscribeTo(event)
	}
}
