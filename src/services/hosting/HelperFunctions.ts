import { IHosting, IHostCategory, IUserSettings } from '../../database/models/SettingsModel'
import App from './App'

export async function findUser (app: App, ignorePastHosted = false): Promise<IHosting> {
	const ignoredChannelIds = app.dontHostList(ignorePastHosted)

	app.logMessage('DEBUG findUser() -> Finding a user to host. Ignored channels:', ignoredChannelIds)

	let ratingCheck: string[]
	switch (app.maxHostRating) {
		case 'family-friendly':
			ratingCheck = [ 'family' ]
			break
		case 'teen':
			ratingCheck = [ 'teen', 'family' ]
			break
		default:
			ratingCheck = [ '18%2B', 'teen', 'family' ]
			break
	}

	const sortedHostingStyles = app.hostCategories
	let userToHost: IHosting = null

	await asyncForEach(sortedHostingStyles, async (style: IHostCategory) => {
		if (userToHost === null) {
			if (style.key === 'HIGHPRIORITYCHANNELLIST')
				userToHost = await findUserFromChannelsList(app, app.dontHostList(true), ratingCheck, true)
			else if (style.key === 'CHANNELLIST')
				userToHost = await findUserFromChannelsList(app, ignoredChannelIds, ratingCheck, false)
			else if (style.key === 'TEAMLIST') userToHost = await findUserFromTeamsList(app, ignoredChannelIds, ratingCheck)
			else if (style.key === 'FOLLOWEDPARTNERS' || style.key === 'FOLLOWING')
				userToHost = await findUserFromFollowingList(
					app,
					ignoredChannelIds,
					ratingCheck,
					style.key === 'FOLLOWEDPARTNERS'
				)
		}
	})

	return userToHost
}

async function findUserFromChannelsList (
	app: App,
	ignoredChannels: number[],
	ratingCheck: string[],
	prioritizedList: boolean
): Promise<IHosting> {
	try {
		let list = []

		if (prioritizedList === true) {
			list = app.priorityChannelsList
			app.logMessage(
				'INFO findUserFromChannelsList() -> Finding user from channels list using the high priority. List length:',
				list.length
			)
		} else {
			list = app.channelsList
			app.logMessage(
				'INFO findUserFromChannelsList() -> Finding user from channels list using the normal list. List length:',
				list.length
			)
		}

		let channels = list.filter((user) => ignoredChannels.includes(user.channelid) === false)

		if (!channels || channels.length === 0) return Promise.resolve(null)

		const copyOfChannels = [ ...channels ]

		const channelids = channels.map((channel) => channel.channelid)

		if (channelids.length === 0) return Promise.resolve(null)

		const size = 100
		const allChannels = []
		for (let i = 0; i < channelids.length; i += size) {
			allChannels.push(channelids.slice(i, i + size).join(';'))
		}

		let target = null
		await asyncForEach(allChannels, async (channelids) => {
			if (target) return

			let uri = `https://mixer.com/api/v1/channels?fields=token,id&limit=100&where=online:eq:true,audience:in:${ratingCheck.join(
				';'
			)},id:in:${channelids}`

			if (app.gamesList.length > 0) uri += ',typeId:in:' + app.gamesList.map((game) => game.typeid).join(';')

			app.logMessage('DEBUG findUserFromChannelsList() -> GET Request to:', uri)

			const response: any = await app.clientRequest({
				method: 'GET',
				uri
			})

			app.logMessage('DEBUG findUserFromChannelsList() -> Successfully got channels. Channels Found:', response.length)

			let onlineUsers = copyOfChannels.filter(
				(channel) => response.find((user) => user.id === channel.channelid) !== undefined
			)

			if (onlineUsers.length !== 0) {
				const found = onlineUsers[0]

				target = {
					channelid: found.channelid,
					username: found.token,
					manual: false,
					hostedAt: null
				}
			}
		})

		return Promise.resolve(target)
	} catch (error) {
		app.logMessage('ERROR findUserFromChannelsList() -> Could not find user to host. Error:', error)

		return Promise.resolve(null)
	}
}

async function findUserFromTeamsList (app: App, ignoredChannels: number[], ratingCheck: string[]): Promise<IHosting> {
	try {
		const teams = app.teamsList

		let userFound: IHosting = null
		await asyncForEach(teams, async (team) => {
			if (userFound) return

			let uri = `https://mixer.com/api/v1/teams/${team.id}/users?fields=username,id&limit=100&where=channel.online:eq:true,teamMembership.accepted:eq:true,channel.audience:in:${ratingCheck.join(
				';'
			)}`

			app.logMessage('DEBUG findUserFromTeamsList() -> GET Request to:', uri)

			let users: any = await app.clientRequest({
				method: 'GET',
				uri
			})

			app.logMessage('DEBUG findUserFromTeamsList() -> Successfully got teams. Response length:', users.length)

			users = users.filter((user) => ignoredChannels.includes(user.channel.id) === false)

			if (app.gamesList && app.gamesList.length > 0)
				users = users.filter((user) => app.gamesList.map((game) => game.typeid).includes(user.channel.typeId))

			if (users.length !== 0) {
				const target = users[Math.floor(Math.random() * users.length)]

				userFound = {
					channelid: target.channel.id,
					username: target.username,
					hostedAt: null,
					manual: null
				}
			}
		})

		return Promise.resolve(userFound)
	} catch (error) {
		app.logMessage('ERROR findUserFromTeamsList() -> Could not find user from TEAMLIST. Error:', error)

		return Promise.resolve(null)
	}
}

async function findUserFromFollowingList (
	app: App,
	ignoredChannels: number[],
	ratingCheck: string[],
	partnered = false
): Promise<IHosting> {
	try {
		let uri = `https://mixer.com/api/v1/users/${app.user
			.userid}/follows?fields=id,token&limit=100&where=audience:in:${ratingCheck.join(';')},online:eq:true${partnered
			? ',partnered:eq:true'
			: ''}`

		if (app.gamesList.length > 0) uri += ',typeId:in:' + app.gamesList.map((game) => game.typeid).join(';')

		app.logMessage('DEBUG findUserFromFollowingList() -> GET Request to:', uri)

		let users: any = await app.clientRequest({
			method: 'GET',
			uri
		})

		app.logMessage(
			'DEBUG findUserFromFollowingList() -> Successfully got followed users. Response length:',
			users.length
		)

		users = users.filter((channel) => ignoredChannels.includes(channel.id) === false)

		if (users.length === 0) return Promise.resolve(null)

		const target = users[Math.floor(Math.random() * users.length)]

		return Promise.resolve({
			channelid: target.id,
			username: target.token,
			hostedAt: null,
			manual: null
		})
	} catch (error) {
		app.logMessage('ERROR findUserFromFollowingList() -> Could not find user from FOLLOWINGLIST. Error:', error)

		return Promise.resolve(null)
	}
}

async function findUserFromPartnerList (app: App, ignoredChannels: number[], ratingCheck: string[]): Promise<IHosting> {
	try {
		let uri = `https://mixer.com/api/v1/channels?fields=id,token&limit=100&where=partnered:eq:true,audience:in:${ratingCheck.join(
			';'
		)}`

		if (app.gamesList.length > 0) uri += ',typeId:in:' + app.gamesList.map((game) => game.typeid).join(';')

		app.logMessage('DEBUG findUserFromPartnerList() -> GET Request to:', uri)

		let users: any = await app.clientRequest({
			method: 'GET',
			uri
		})

		app.logMessage('DEBUG findUserFromPartnerList() -> Successfully got partners list. Response length:', users.length)

		users = users.filter((channel) => ignoredChannels.includes(channel.id) === false)

		if (users.length === 0) return Promise.resolve(null)

		const target = users[Math.floor(Math.random() * users.length)]

		return Promise.resolve({
			channelid: target.id,
			username: target.token,
			hostedAt: null,
			manual: null
		})
	} catch (error) {
		app.logMessage('ERROR findUserFromPartnerList() -> Could not find user from PARTNERSLIST. Error:', error)

		return Promise.resolve(null)
	}
}

export async function asyncForEach (array, callback) {
	for (let index = 0; index < array.length; index++) {
		await callback(array[index], index, array)
	}
}

export function getValidatedSettings (settings: IUserSettings) {
	if (settings.hostChanging.rehostTimeout !== 0) {
		if (settings.hostChanging.rehostTimeout < 60) settings.hostChanging.rehostTimeout = 60
		if (settings.hostChanging.rehostTimeout > 900) settings.hostChanging.rehostTimeout = 900
	}

	settings.hostMessaging.shouldWhisper = true

	return settings
}
