export class UnauthorizedChatError extends Error {
	constructor (message) {
		super(message)

		this.name = this.constructor.name

		Error.captureStackTrace(this, this.constructor)
	}
}

export class NoUserError extends Error {
	constructor (message) {
		super(message)

		this.name = this.constructor.name

		Error.captureStackTrace(this, this.constructor)
	}
}

export class AppOfflineError extends Error {
	constructor (message) {
		super(message)

		this.name = this.constructor.name

		Error.captureStackTrace(this, this.constructor)
	}
}

export class MixerError extends Error {
	constructor (message) {
		super(message)

		this.name = this.constructor.name

		Error.captureStackTrace(this, this.constructor)
	}
}

export class RefreshTokenNotValidError extends Error {
	constructor (message) {
		super(message)

		this.name = this.constructor.name

		Error.captureStackTrace(this, this.constructor)
	}
}

export class UserOnlineError extends Error {
	constructor (message) {
		super(message)

		this.name = this.constructor.name

		Error.captureStackTrace(this, this.constructor)
	}
}

export class UserError extends Error {
	constructor (message) {
		super(message)

		this.name = this.constructor.name

		Error.captureStackTrace(this, this.constructor)
	}
}
