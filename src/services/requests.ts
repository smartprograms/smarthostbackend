const request = require('retry-request')

var os = require('os')

var interfaces = os.networkInterfaces()
var localAddresses = []
for (var k in interfaces) {
	for (var k2 in interfaces[k]) {
		var address = interfaces[k][k2]
		if (address.family === 'IPv4' && !address.internal) {
			localAddresses.push(address.address)
		}
	}
}

let addressToUse = 0

export function apiRequest (options, callback) {
	if (localAddresses.length !== 1) {
		if (addressToUse + 1 >= localAddresses.length) addressToUse = 0
		else addressToUse += 1

		options.localAddress = localAddresses[addressToUse]
	}

	options.json = true

	request(
		options,
		{
			retries: 5
		},
		(err, resp, body) => {
			if (err) return callback(err)

			if (resp.statusCode !== 200)
				return callback({
					status: resp.statusCode,
					error: 'Request Error',
					message: 'Could not get the requested resource.',
					response: resp
				})

			return callback(null, body, options.localAddress)
		}
	)
}
