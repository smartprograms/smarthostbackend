import * as _ from 'lodash'
import * as express from 'express'
import { Admin, IUserRequest } from '../auth/verify'
import { apiRequest } from '../../../requests'
import { db } from '../../../../database/services/DatabaseService'
import { getAppStatus, forceHost, forceShutdown, forceRestart } from '../../../hosting/AppManager'
import { getSubscription } from '../payments/stripeHelper'
import { getPixelUser } from '../../../Business/pixel'

const adminUserRouter = express.Router()

adminUserRouter.use(Admin)

adminUserRouter.get('/pixel-update', (req: IUserRequest, res) => {
	getPixelUser()

	res.status(200).json({
		working: true
	})
})

adminUserRouter.get('/', async (req: IUserRequest, res) => {
	try {
		res.status(200).json({
			hello: 'admin'
		})
	} catch (error) {
		serverError(res)
	}
})

adminUserRouter.get('/force/restart/:username', async (req: IUserRequest, res) => {
	try {
		const username = req.params.username

		const opts = {
			method: 'GET',
			uri: 'https://mixer.com/api/v1/channels/' + username
		}

		apiRequest(opts, (err, response) => {
			if (err) throw err

			if (!response) throw 'no res'

			const channelid = response.id

			forceRestart(channelid)

			res.status(200).send({
				working: true
			})
		})
	} catch (error) {
		serverError(res)
	}
})

adminUserRouter.get('/force/:username', async (req: IUserRequest, res) => {
	try {
		const username = req.params.username

		const opts = {
			method: 'GET',
			uri: 'https://mixer.com/api/v1/channels/' + username
		}

		apiRequest(opts, (err, response) => {
			if (err) throw err

			if (!response) throw 'no res'

			const channelid = response.id

			forceHost(channelid)

			res.status(200).send({
				working: true
			})
		})
	} catch (error) {
		serverError(res)
	}
})

adminUserRouter.get('/user/:username', (req: IUserRequest, res) => {
	try {
		const username = req.params.username

		const opts = {
			method: 'GET',
			uri: 'https://mixer.com/api/v1/channels/' + username
		}

		apiRequest(opts, (err, response) => {
			if (err) throw err

			if (!response) throw 'no res'

			db
				.getUserSettingsById(response.id)
				.then((settings) => {
					const reply = {
						settings,
						appStatus: getAppStatus(response.id),
						hosting: settings.hostStatus
					}

					res.status(200).json(reply)
				})
				.catch(() => {
					serverError(res)
				})
		})
	} catch (error) {
		serverError(res)
	}
})

adminUserRouter.get('/ban/:username', (req: IUserRequest, res) => {
	try {
		const username = req.params.username

		const opts = {
			method: 'GET',
			uri: 'https://mixer.com/api/v1/channels/' + username
		}

		apiRequest(opts, async (err, response) => {
			if (err) throw err

			if (!response) throw 'no res'

			const user = await db.getUserById(response.id)

			user.banned = true

			db.addOrUpdateUser(user, (err) => {
				if (err) console.error('Could not ban user error:', err)
			})

			forceShutdown(user.channelid)

			res.status(200).json({ banning: req.params.username })
		})
	} catch (error) {
		serverError(res)
	}
})

adminUserRouter.get('/unban/:username', (req: IUserRequest, res) => {
	try {
		const username = req.params.username

		const opts = {
			method: 'GET',
			uri: 'https://mixer.com/api/v1/channels/' + username
		}

		apiRequest(opts, async (err, response) => {
			if (err) throw err

			if (!response) throw 'no res'

			const user = await db.getUserById(response.id)

			user.banned = false

			db.addOrUpdateUser(user, (err) => {
				if (err) console.error('Could not unban user error:', err)
			})

			res.status(200).json({ unbanning: req.params.username })
		})
	} catch (error) {
		serverError(res)
	}
})

adminUserRouter.get('/subscription/:username', (req: IUserRequest, res) => {
	try {
		const username = req.params.username

		const opts = {
			method: 'GET',
			uri: 'https://mixer.com/api/v1/channels/' + username
		}

		apiRequest(opts, (err, response) => {
			if (err) throw err

			if (!response) throw 'no res'

			db
				.getUserById(response.id)
				.then(async (user) => {
					user.tokens = null

					let subscription = null

					if (user.stripeId) subscription = await getSubscription(user.stripeId)

					const reply = {
						user,
						subscription
					}

					res.status(200).json(reply)
				})
				.catch(() => {
					serverError(res)
				})
		})
	} catch (error) {
		serverError(res)
	}
})

function serverError (res) {
	res.status(500).json({
		statusCode: 500,
		error: 'Internal Server Error',
		message: 'The server is having trouble processing your request, try again later.'
	})
}

export default adminUserRouter
