import * as express from 'express'
import * as jwt from 'jsonwebtoken'
import oauth2 = require('simple-oauth2')
import { db } from '../../../../database/services/DatabaseService'
import { apiRequest } from '../../../requests'
import { IMixerUser } from '../../../../database/models/UserModel'

const oauth = oauth2.create({
	client: {
		id: process.env.MIXER_CLIENTID,
		secret: process.env.MIXER_SECRETID
	},
	auth: {
		tokenHost: 'https://mixer.com/api/v1',
		authorizeHost: 'https://mixer.com'
	},
	options: {
		authorizationMethod: 'body'
	}
})

const scope = process.env.MIXER_SCOPES.split(', ').join(' ')
const redirect_uri = process.env.MIXER_CALLBACK

const authorizationUri = oauth.authorizationCode.authorizeURL({
	redirect_uri,
	scope,
	state: 'authorize'
})

const authRouter = express.Router()

const secret = process.env.JWT_SECRET

authRouter.get('/mixer', (req, res) => res.redirect(authorizationUri))

authRouter.get('/mixer/callback', async (req, res) => {
	const query = req.query

	if (query && query.code) {
		try {
			const response = await oauth.authorizationCode.getToken({
				code: query.code,
				redirect_uri
			})

			const user = (await getUser(response.access_token)) as any

			const updateUser = () => {
				const mixerUser: IMixerUser = {
					channelid: user.channel.id,
					userid: user.id,
					username: user.username,
					email: user.email,

					tokens: {
						access: response.access_token,
						refresh: response.refresh_token,
						expires: (Date.now() + 1000 * 60 * 60 * 6) / 1000
					}
				}

				db.addOrUpdateUser(mixerUser, (error, userObject) => {
					if (error) throw error

					userObject.tokens = null
					userObject.stripeId = null

					const token = jwt.sign(userObject, secret, {
						expiresIn: '6h'
					})

					res.redirect(process.env.APP_REDIRECT + '?jwt=' + token)
				})
			}

			db
				.getUserById(user.channel.id)
				.then((dbUser) => {
					if (dbUser.banned) {
						res.redirect(process.env.APP_REDIRECT + '?banned=true')
					} else {
						updateUser()
					}
				})
				.catch((err) => {
					if (err === 'No data?') updateUser()
					else res.redirect(process.env.APP_REDIRECT + '?auth=failed')
				})
		} catch (error) {
			console.error(error)
			res.redirect(process.env.APP_REDIRECT + '?auth=failed')
		}
	} else if (query.error) {
		res.redirect(process.env.APP_REDIRECT + '?auth=failed')
	} else {
		res.redirect(authorizationUri)
	}
})

function getUser (access: string) {
	const options = {
		method: 'GET',
		uri: 'https://mixer.com/api/v1/users/current',
		headers: {
			'Client-ID': process.env.MIXER_CLIENTID,
			Authorization: 'Bearer ' + access
		}
	}
	return new Promise((resolve, reject) => {
		apiRequest(options, (error, res) => {
			if (error) reject(error)
			else resolve(res)
		})
	})
}

export default authRouter
