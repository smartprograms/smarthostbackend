import * as jwt from 'jsonwebtoken'
import * as express from 'express'
import { db } from '../../../../database/services/DatabaseService'
import { IMixerUser } from '../../../../database/models/UserModel'

const secret = process.env.JWT_SECRET

export function Authenticated (req, res, next) {
	const Authorization = req.get('Authorization')
	if (!Authorization)
		return res.status(401).json({
			status: 401,
			error: 'Unauthorized',
			message: 'You must be authenticated to see this resource'
		})

	jwt.verify(Authorization, secret, async (err, decoded) => {
		if (err) {
			console.error(err)
			return res.status(401).json({
				status: 401,
				error: 'Unauthorized',
				message: 'Invalid_Auth_Token, Try to logout and back in'
			})
		} else {
			db
				.getUserById(decoded.channelid)
				.then((user) => {
					req.user = user
					if (user.banned)
						return res.status(403).json({
							status: 403,
							error: 'Forbidden',
							message: 'You are not allowed to do that action'
						})

					return next()
				})
				.catch((err) => {
					return res.status(500).json({
						status: 500,
						error: 'Internal Server Error',
						message: 'There is an error with our server, please try again later'
					})
				})
		}
	})
}

export function Admin (req, res, next) {
	const Authorization = req.get('Authorization')
	if (!Authorization)
		return res.status(401).json({
			status: 401,
			error: 'Unauthorized',
			message: 'You must be authenticated to see this resource.'
		})

	jwt.verify(Authorization, secret, async (err, decoded) => {
		if (err || !decoded.admin) {
			return res.status(401).json({
				status: 401,
				error: 'Unauthorized',
				message: "You're a bot m8"
			})
		} else {
			req.user = decoded
			return next()
		}
	})
}

export interface IUserRequest extends express.Request {
	user: IMixerUser
}
