import * as _ from 'lodash'
import * as express from 'express'
import { getProPlan, getSubscription, subscribeToPlan, getDiscountFromCode } from './stripeHelper'
import { Authenticated, IUserRequest } from '../auth/verify'
import { db } from '../../../../database/services/DatabaseService'
import { removeProFeatures, updateProStatus } from './proHandler'

import * as Stripe from 'stripe'
const stripe = new Stripe(process.env.STRIPE_SECRET)

const paymentsRouter = express.Router()

const endpointSecret = process.env.STRIPE_HOOK_SECRET
paymentsRouter.post('/subscription/hook', async (req, res) => {
	try {
		const sig = req.headers['stripe-signature']

		const event = stripe.webhooks.constructEvent((req as any).rawBody, sig, endpointSecret)

		if (event.type !== 'customer.subscription.deleted') throw new Error('WrongEvent')

		const channelid = (event.data.object as any).metadata.channelid

		if (!channelid) throw new Error('UserNoChannel')

		await removeProFeatures(channelid)

		return res.status(200).json({ received: true })
	} catch (err) {
		console.error('Error at webhook')
		console.error('Webhook raw body:', (req as any).rawBody)
		console.error('Webhook error message:', err.message)

		if ([ 'WrongEvent', 'UserNoChannel' ].includes(err.message))
			return res.status(400).send(`Webhook Error: ${err.message}`)

		return serverError(res)
	}
})

paymentsRouter.use(Authenticated)

paymentsRouter.get('/coupon/:code', async (req: IUserRequest, res) => {
	try {
		if (!req.params.code) return res.status(400).json({ error: 'INVALID_CODE' })

		const reply = await getDiscountFromCode(req.params.code)

		res.status(200).json(reply)
	} catch (error) {
		console.error(error)

		serverError(res, error.statusCode, error.type, error.message)
	}
})

paymentsRouter.get('/subscription/plans', async (req, res) => {
	try {
		const plans = (await getProPlan()) as any

		res.status(200).json(plans)
	} catch (error) {
		console.error(error)
		serverError(res, error.statusCode, error.type, error.message)
	}
})

paymentsRouter.get('/subscription/current', async (req: IUserRequest, res) => {
	try {
		if (!req.user.stripeId) {
			updateProStatus(req.user.channelid, false)
			return res.status(200).json({ subscription: null })
		}

		const subscription = process.env.NODE_ENV === 'dev' ? null : await getSubscription(req.user.stripeId)

		updateProStatus(req.user.channelid, !!subscription)

		return res.status(200).json({ subscription })
	} catch (error) {
		if (error === 'StripeUserDeleted' && process.env.NODE_ENV !== 'dev') {
			req.user.stripeId = null

			db.addOrUpdateUser(req.user, () => {
				return res.status(200).json({ subscription: null })
			})
		} else {
			return serverError(res)
		}
	}
})

paymentsRouter.get('/subscription/end', async (req: IUserRequest, res) => {
	try {
		if (!req.user.stripeId) throw 'No stripe id'

		const currentSub = await getSubscription(req.user.stripeId)

		if (!currentSub) throw 'Not currently subscribed'

		const subscription = await stripe.subscriptions.update(currentSub.id, { cancel_at_period_end: true })

		return res.status(200).json({ subscription })
	} catch (error) {
		return serverError(res)
	}
})

paymentsRouter.post('/subscription/start', async (req: IUserRequest, res) => {
	try {
		if (!req.body.token) throw 'Card was invalid'
		if (!req.body.plan) throw 'Could not retrieve the plan to subscribe to'
		if (!req.user.email) throw 'Could not get your accounts email'

		if (req.user.stripeId) {
			const subscription = await getSubscription(req.user.stripeId)

			if (subscription) return res.status(200).json({ subscription })

			console.log('No previous sub')

			const source = await stripe.customers.createSource(req.user.stripeId, {
				source: req.body.token
			})

			if (!source) throw 'Stripe Payments Error'

			const subscribe = await subscribeToPlan(req.body.plan, req.user, source.id, req.body.code)

			return res.status(200).json(subscribe)
		} else {
			console.log('No stripe id')

			const customer = await stripe.customers.create({
				email: req.user.email,
				source: req.body.token,
				metadata: {
					channelid: req.user.channelid
				}
			})

			if (!customer) throw 'Stripe Customer Error'

			req.user.stripeId = customer.id

			db.addOrUpdateUser(req.user, async (err, user) => {
				if (err) return serverError(res)

				console.log(user.username + ' should now have a stripe id of ' + user.stripeId)

				subscribeToPlan(req.body.plan, req.user, undefined, req.body.code)
					.then((subscribe) => {
						return res.status(200).json(subscribe)
					})
					.catch((error) => {
						console.error(error)

						return serverError(res, error.statusCode, error.type, error.message)
					})
			})
		}
	} catch (error) {
		if (error === 'StripeUserDeleted' && process.env.NODE_ENV !== 'dev') {
			req.user.stripeId = null
			db.addOrUpdateUser(req.user, () => {})
		}

		console.error(error)

		return serverError(res, error.statusCode, error.type, error.message)
	}
})

function serverError (
	res,
	code = 500,
	error = 'Internal Server Error',
	message = 'The server is having trouble processing your request, try again later.'
) {
	return res.status(code).json({
		statusCode: code,
		error: error,
		message: message
	})
}

export default paymentsRouter
