import { db } from '../../../../database/services/DatabaseService'
import { getSubscription } from './stripeHelper'
import { CacheService } from '../../../../database/services/CacheService'

export async function removeProFeatures (channelid) {
	try {
		const settings = await db.getUserSettingsById(channelid)

		let modified = false

		if (settings.hostCategory.length > Number(process.env.HOSTCATEGORY_MAX)) {
			let temp = settings.hostCategory.filter((category) => {
				return category.key !== 'HIGHPRIORITYCHANNELLIST'
			})

			if (temp.length !== 0) {
				temp.map((cat, i) => {
					cat.priority = i
				})

				temp = temp.filter((category) => {
					return category.priority < Number(process.env.HOSTCATEGORY_MAX)
				})
			}

			settings.hostCategory = temp

			modified = true
		}

		if (settings.channelList.length > Number(process.env.CHANNELLIST_MAX)) {
			const temp = settings.channelList.filter((channel) => {
				return channel.priority < Number(process.env.CHANNELLIST_MAX)
			})

			settings.channelList = temp

			modified = true
		}

		if (settings.teamList.length > Number(process.env.TEAMLIST_MAX)) {
			const temp = settings.teamList.filter((team) => {
				team.priority < Number(process.env.TEAMLIST_MAX)
			})

			settings.teamList = temp

			modified = true
		}

		if (settings.hostMessaging.shouldWhisper === false) {
			settings.hostMessaging.shouldWhisper = true
			modified = true
		}

		const promo = '(hosting provided by smarthostapp)'

		if (settings.hostMessaging.sendMessage) {
			if (!settings.hostMessaging.message.endsWith(promo)) {
				settings.hostMessaging.message = settings.hostMessaging.message + ` ${promo}`

				if (settings.hostMessaging.message.length > 360) {
					let subLength = 360 - 3 - promo.length - 1

					settings.hostMessaging.message = settings.hostMessaging.message.replace(promo, '')

					settings.hostMessaging.message = settings.hostMessaging.message.substring(0, subLength) + '... ' + promo
				}

				modified = true
			}
		}

		if (settings.hostMessaging.sendEndMessage) {
			if (!settings.hostMessaging.endMessage.endsWith(promo)) {
				settings.hostMessaging.endMessage = settings.hostMessaging.endMessage + ` ${promo}`

				if (settings.hostMessaging.endMessage.length > 360) {
					let subLength = 360 - 3 - promo.length - 1

					settings.hostMessaging.endMessage = settings.hostMessaging.endMessage.replace(promo, '')

					settings.hostMessaging.endMessage = settings.hostMessaging.endMessage.substring(0, subLength) + '... ' + promo
				}

				modified = true
			}
		}

		if (modified) {
			db.addOrUpdateUserSettings(settings, (err) => {
				if (err) return Promise.reject(err)

				return Promise.resolve()
			})
		} else return Promise.resolve()
	} catch (error) {
		return Promise.reject(error)
	}
}

const cache = new CacheService(60 * 60 * 12)

export function checkPro (channelid): Promise<boolean> {
	return new Promise((resolve, reject) => {
		const key = 'subscribed_' + channelid

		const getCurrentStatus = async () => {
			try {
				const user = await db.getUserById(channelid)

				if (!user || !user.stripeId) {
					cache.set(key, false)
					return resolve(false)
				} else {
					const subscription = await getSubscription(user.stripeId)

					cache.set(key, !!subscription)
					return resolve(!!subscription)
				}
			} catch (error) {
				return reject(error)
			}
		}

		cache.get(key).then((subbed: boolean) => resolve(subbed)).catch(() => {
			getCurrentStatus()
		})
	})
}

export function updateProStatus (channelid, status) {
	const key = 'subscribed_' + channelid
	cache.set(key, status)
}
