import { CacheService } from '../../../../database/services/CacheService'

import * as Stripe from 'stripe'
import { updateProStatus } from './proHandler'
import { IMixerUser } from '../../../../database/models/UserModel'
const stripe = new Stripe(process.env.STRIPE_SECRET)

const cache = new CacheService(60 * 60)

export function getProPlan () {
	return new Promise(async (resolve, reject) => {
		try {
			let cached = await cache.get('plans')

			resolve(cached)
		} catch (error) {
			stripe.plans.list({ active: true }, (err, plans) => {
				if (err) return reject(err)

				cache.set('plans', plans.data)

				return resolve(plans.data)
			})
		}
	})
}

export async function getCustomer (customerId) {
	try {
		const customer = await stripe.customers.retrieve(customerId)

		if (!customer || (customer as any).deleted) throw new Error('StripeUserDeleted')

		return customer
	} catch (error) {
		return Promise.reject(error)
	}
}

export async function getSubscription (customer) {
	try {
		const res = await getCustomer(customer)

		if (!res || res.subscriptions.data.length === 0) return null

		const sub = res.subscriptions.data.find((subscription) => {
			return subscription.status === 'active' || subscription.status === 'trialing'
		})

		return Promise.resolve(sub)
	} catch (error) {
		return Promise.reject(error.message || error)
	}
}

export async function subscribeToPlan (plan, user: IMixerUser, paymentId, coupon?) {
	try {
		const subscription = await stripe.subscriptions.create({
			customer: user.stripeId,
			items: [ { plan } ],
			payment_behavior: 'error_if_incomplete',
			default_payment_method: paymentId,
			coupon
		} as any)

		updateProStatus(user.channelid, subscription.status === 'active' || subscription.status === 'trialing')

		return Promise.resolve({ subscription })
	} catch (error) {
		updateProStatus(user.channelid, false)
		return Promise.reject(error)
	}
}

export async function getDiscountFromCode (code) {
	try {
		const coupon = await stripe.coupons.retrieve(code)

		const type = coupon.amount_off ? 'amount' : 'percent'

		return Promise.resolve({ type, discount: coupon.amount_off || coupon.percent_off })
	} catch (error) {
		return Promise.reject(error)
	}
}
