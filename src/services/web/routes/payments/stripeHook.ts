import * as express from 'express'
import { removeProFeatures } from './proHandler'
import { getCustomer } from './stripeHelper'

import * as Stripe from 'stripe'
const stripe = new Stripe(process.env.STRIPE_SECRET)

const stripeHooksRouter = express.Router()

const endpointSecret = process.env.STRIPE_HOOK_SECRET
stripeHooksRouter.post('/subscription/deleted', async (req, res) => {
	try {
		const sig = req.headers['stripe-signature']

		const event = stripe.webhooks.constructEvent(req.body, sig, endpointSecret)

		if (event.type !== 'customer.subscription.deleted') throw new Error('WrongEvent')

		const customerId = (event.data.object as any).customer

		if (!customerId) throw new Error('SubscriptionNoCustomer')

		const customer = await getCustomer(customerId)

		const channelid = parseInt(customer.metadata.channelid)

		if (!channelid) throw new Error('UserNoChannel')

		await removeProFeatures(channelid)

		return res.status(200).json({ received: true })
	} catch (err) {
		console.error('Error at webhook')
		console.error('Webhook raw body:', req.body)
		console.error('Webhook error message:', err.message)

		if ([ 'WrongEvent', 'SubscriptionNoCustomer', 'UserNoChannel' ].includes(err.message))
			return serverError(res, 400, 'Bad Request', err.message)

		return serverError(res, 500, 'Internal Server Error', err.message)
	}
})

function serverError (
	res,
	code = 500,
	error = 'Internal Server Error',
	message = 'The server is having trouble processing your request, try again later.'
) {
	return res.status(code).json({
		statusCode: code,
		error: error,
		message: message
	})
}

export default stripeHooksRouter
