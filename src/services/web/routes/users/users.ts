import * as _ from 'lodash'
import * as express from 'express'
import { db } from '../../../../database/services/DatabaseService'
import { IUserSettings } from '../../../../database/models/SettingsModel'
import { Authenticated } from '../auth/verify'
import { getAppStatus } from '../../../hosting/AppManager'
import { checkPro } from '../payments/proHandler'
import { IMixerUser } from '../../../../database/models/UserModel'
import logger from '../../../log'
import { celebrate, Joi, errors } from 'celebrate'

const linkify = require('linkifyjs')

const userRouter = express.Router()

userRouter.use(Authenticated)

userRouter.get('/current', async (req: IUserRequest, res) => {
	try {
		const user = await db.getUserById(req.user.channelid)

		res.status(200).json(user)
	} catch (error) {
		serverError(res)
	}
})

userRouter.get('/current/status', async (req: IUserRequest, res) => {
	try {
		const settings = await db.getUserSettingsById(req.user.channelid)

		const reply = {
			offlineAt: settings.hostStatus.offlineAt
				? Math.abs(
						Math.round((new Date().getTime() - new Date(settings.hostStatus.offlineAt).getTime()) / 1000 / (60 * 60))
					)
				: null,
			hosting: settings.hostStatus.hosted,
			hostedAt: settings.hostStatus.hostedAt
				? Math.abs(Math.round((new Date().getTime() - new Date(settings.hostStatus.hostedAt).getTime()) / 1000 / 60))
				: null,
			hostCheck: settings.hostStatus.offlineAt
				? settings.hostChanging.rehostTimeout
				: settings.hostChanging.hostAfterOfflineTimeout,
			appStatus: getAppStatus(req.user.channelid)
		}

		res.status(200).json(reply)
	} catch (error) {
		serverError(res)
	}
})

userRouter.get('/current/settings', async (req: IUserRequest, res) => {
	try {
		const settings = await db.getUserSettingsById(req.user.channelid)

		res.status(200).json(settings)
	} catch (error) {
		serverError(res)
	}
})

userRouter.post('/current/deactivate', async (req: IUserRequest, res) => {
	try {
		const settings = await db.getUserSettingsById(req.user.channelid)

		if (settings.active === false) return res.status(200).json(settings)

		settings.active = false

		db.addOrUpdateUserSettings(settings, (error) => {
			if (error) throw error

			return res.status(200).json(settings)
		})
	} catch (error) {
		console.error('Error updating settings: ', error)

		return serverError(res)
	}
})

function validateSettings () {
	return {
		body: Joi.object().keys({
			active: Joi.bool().required(),
			channelid: Joi.number().required(),

			highPriorityChannelList: Joi.array()
				.items(
					Joi.object().keys({
						channelid: Joi.number().required(),
						token: Joi.string().required(),
						priority: Joi.number().required()
					})
				)
				.required(),

			channelList: Joi.array()
				.items(
					Joi.object().keys({
						channelid: Joi.number().required(),
						token: Joi.string().required(),
						priority: Joi.number().required()
					})
				)
				.required(),
			randomChannelOrder: Joi.bool().required(),

			teamList: Joi.array()
				.items(
					Joi.object().keys({
						id: Joi.number().required(),
						name: Joi.string().required(),
						priority: Joi.number().required()
					})
				)
				.required(),
			randomTeamOrder: Joi.bool().required(),

			hostCategory: Joi.array()
				.min(1)
				.items(
					Joi.object().keys({
						key: Joi.any().required(),
						name: Joi.string().required(),
						priority: Joi.number().required()
					})
				)
				.required()
				.error(new Error('You must select at least 1 host category.')),

			hostChanging: Joi.object()
				.keys({
					rehostTimeout: Joi.number()
						.allow(0)
						.min(60)
						.required()
						.error(new Error('The host rotation time must be a minimum of 60 minutes.')),
					rehostManual: Joi.bool().required(),
					hostAfterOfflineTimeout: Joi.number().required(),
					maxRating: Joi.string().valid('18+', 'teen', 'family-friendly').required(),
					hostGames: Joi.array()
						.items(
							Joi.object().keys({
								typeid: Joi.number().required(),
								name: Joi.string().required()
							})
						)
						.required(),
					useGamesList: Joi.bool().required()
				})
				.required(),

			hostMessaging: Joi.object()
				.keys({
					message: Joi.string()
						.max(160)
						.required()
						.error(new Error('Your message cannot contain more than 160 characters.')),
					sendMessage: Joi.bool().required()
				})
				.unknown()
				.required(),

			channelExclusion: Joi.array()
				.items(
					Joi.object().keys({
						channelid: Joi.number().required(),
						token: Joi.string().required()
					})
				)
				.required(),

			hostStatus: Joi.any(),
			previousHostStatus: Joi.any()
		})
	}
}

userRouter.post('/current/settings', celebrate(validateSettings()), async (req: IUserRequest, res) => {
	let settings = null,
		subbed = false
	try {
		const body = req.body as IUserSettings

		settings = await db.getUserSettingsById(req.user.channelid)
		subbed = await checkPro(req.user.channelid)

		logger.log({
			level: 'UPDATE',
			message: 'Updating settings',
			user: settings.channelid,
			pro: subbed
		})

		settings.active = body.active

		settings.randomChannelOrder = body.randomChannelOrder
		settings.randomTeamOrder = body.randomTeamOrder

		if (body.hostCategory.length > Number(process.env.HOSTCATEGORY_MAX) && !subbed)
			return notPro(res, `Only ${process.env.HOSTCATEGORY_MAX} Host Styles Allowed`)

		const newCategories = [ ...body.hostCategory ].filter((cat) => {
			return cat.key !== 'HIGHPRIORITYCHANNELLIST'
		})

		if (!_.isEqual(body.hostCategory, newCategories) && !subbed)
			return notPro(res, `You cannot use the high priority channel list`)

		settings.hostCategory = body.hostCategory

		if (body.channelList.length > Number(process.env.CHANNELLIST_MAX) && !subbed)
			return notPro(res, `Only ${process.env.CHANNELLIST_MAX} Channels Allowed`)

		settings.channelList = body.channelList

		settings.highPriorityChannelList = body.highPriorityChannelList

		if (body.teamList.length > Number(process.env.TEAMLIST_MAX) && !subbed)
			return notPro(res, `Only ${process.env.TEAMLIST_MAX} Teams Allowed`)

		settings.teamList = body.teamList

		settings.hostChanging = body.hostChanging

		settings.hostMessaging = body.hostMessaging

		// always whisper now
		settings.hostMessaging.shouldWhisper = true

		// Fix for self promo
		const links = linkify.find(settings.hostMessaging.message)
		links.forEach((link) => {
			if (link.value) settings.hostMessaging.message.replace(link.value, '')
		})

		const linksEnd = linkify.find(settings.hostMessaging.endMessage)
		linksEnd.forEach((link) => {
			if (link.value) settings.hostMessaging.endMessage.replace(link.value, '')
		})

		// Add promo message for non-subs
		const promo = '(hosting provided by smarthostapp)'

		if (subbed === false) {
			if (!settings.hostMessaging.message.endsWith(promo))
				settings.hostMessaging.message = settings.hostMessaging.message + ` ${promo}`

			if (!settings.hostMessaging.endMessage.endsWith(promo))
				settings.hostMessaging.endMessage = settings.hostMessaging.endMessage + ` ${promo}`

			// Remove public messages from non-pro
			if (settings.hostMessaging.shouldWhisper === false) settings.hostMessaging.shouldWhisper = true
		}

		// Fix for db errors
		if (settings.hostMessaging.endMessage === '') settings.hostMessaging.endMessage = null
		if (settings.hostMessaging.message === '') settings.hostMessaging.message = null

		settings.channelExclusion = body.channelExclusion

		db.addOrUpdateUserSettings(settings, (error) => {
			if (error) throw error

			logger.log({
				level: 'UPDATE',
				message: 'Updated settings success',
				user: settings.channelid,
				pro: subbed
			})

			return res.status(200).json(settings)
		})
	} catch (error) {
		logger.log({
			level: 'UPDATE',
			message: 'Updated settings failed ERROR:' + typeof error === 'object' ? JSON.stringify(error) : error,
			user: settings.channelid,
			pro: subbed
		})

		return serverError(res)
	}
})

userRouter.use(errors())

function notPro (res, from) {
	res.status(403).json({
		statusCode: 403,
		error: 'Forbidden',
		message: 'You must be a pro subscriber to use these settings. (' + from + ')'
	})
}

function serverError (res) {
	res.status(500).json({
		statusCode: 500,
		error: 'Internal Server Error',
		message: 'The server is having trouble processing your request, try again later.'
	})
}

interface IUserRequest extends express.Request {
	user: IMixerUser
}

export default userRouter
