import * as express from 'express'
import * as bodyParser from 'body-parser'
import * as http from 'http'
import authRouter from './routes/auth/auth'
import userRouter from './routes/users/users'
import adminUserRouter from './routes/admin/users'
import paymentsRouter from './routes/payments/payment'
import stripeHooksRouter from './routes/payments/stripeHook'

const app = express()

app.set('etag', false)
app.disable('x-powered-by')

const defaultHeaders = (req, res, next) => {
	res.setHeader('Access-Control-Allow-Origin', '*')
	res.setHeader('Access-Control-Allow-Headers', 'Authorization, Content-Type')
	res.setHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,PATCH,DELETE')

	if (req.method === 'OPTIONS') return res.status(204).json({})

	res.setHeader('Referrer-Policy', 'no-referrer')
	res.setHeader('X-Frame-Options', 'deny')
	res.setHeader('X-XSS-Protection', '1; mode=block;')
	res.setHeader('Strict-Transport-Security', 'max-age=31536000; includeSubDomains')
	res.setHeader('Last-Modified', new Date().toUTCString())

	next()
}

app.use(defaultHeaders)

app.use('/stripe', bodyParser.raw({ type: '*/*' }), stripeHooksRouter)

app.use(bodyParser.json())

app.use('/payments', paymentsRouter)
app.use('/auth', authRouter)
app.use('/users', userRouter)
app.use('/admin', adminUserRouter)

app.use((req, res) => {
	res.status(404).json({
		status: 404,
		error: 'Not Found',
		message: 'The request url was not found.'
	})
})

app.use((err, req, res, next) => {
	console.error(err)
	res.status(500).json({
		status: 500,
		error: 'Internal Server Error',
		message: 'There was an error processing your request please try again later.'
	})
})

export function initWeb () {
	http.createServer(app).listen(process.env.HTTP_PORT, () => {
		console.log('HTTP Server is now listening on port', process.env.HTTP_PORT)
	})
}
